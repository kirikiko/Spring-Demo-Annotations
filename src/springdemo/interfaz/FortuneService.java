package springdemo.interfaz;

public interface FortuneService {

	
	public String getFortune();
	
	public String serveFortune();
}
