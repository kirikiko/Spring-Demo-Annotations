package springdemo.interfaz;

public interface Coach {

	public String getDailyWorkout();
	
	public int daysSpent();

	public String getDailyFortune();
	
	public String serveFortune();
	
}
