package springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import springdemo.impl.CricketCoachImpl;

public class SetterDemoApp {

	public static void main(String[] args) {
		//load the spirng config file
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		//retrieve the bean from the spring container
		CricketCoachImpl theCoachImpl = context.getBean("myCricketCoach", CricketCoachImpl.class);
		
		//call methods on the bean
		System.out.println(theCoachImpl.getDailyFortune());
		
		System.out.println(theCoachImpl.getDailyWorkout());
		
		// call our new methods to get the literal values
		System.out.println(theCoachImpl.getTeam());
		System.out.println(theCoachImpl.getEmailAddress());
		System.out.println(theCoachImpl.serveFortune());
		
		// close the context
		context.close();

	}

}
