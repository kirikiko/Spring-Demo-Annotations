package springdemo;

import springdemo.impl.TrackCoachImpl;
import springdemo.interfaz.Coach;

public class MyApp {

	public static void main(String[] args) {
		
		//create the object
		Coach coach = new TrackCoachImpl();
		
		// use the object
		System.out.println(coach.getDailyWorkout());
		
	}

}
