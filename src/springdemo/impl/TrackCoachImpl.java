package springdemo.impl;

import springdemo.interfaz.Coach;

public class TrackCoachImpl implements Coach {
	
	private FortuneServiceImpl fortuneService;
	
	public TrackCoachImpl() {}
	
	public TrackCoachImpl(FortuneServiceImpl fortuneService) {
		this.fortuneService = fortuneService;
	}

	@Override
	public String getDailyWorkout() {
		return "Run a hard 5K";
	}

	@Override
	public int daysSpent() {
		return 2;
	}

	@Override
	public String getDailyFortune() {
		return "Just do it: " + fortuneService.getFortune();
	}

	@Override
	public String serveFortune() {
		// TODO Auto-generated method stub
		return null;
	}
	
	// add an init method
	public void doMyStartUpStuff() {
		System.out.println("TrackCoach: inside method doMyStartUpStuff");
	}
	
	// add a destroy method
	public void doMyCleanUpStuff() {
		System.out.println("TrackCoach: inside method onDestroy");
	}
	
	

}
