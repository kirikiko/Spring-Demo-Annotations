package springdemo.impl;

import springdemo.interfaz.Coach;
import springdemo.interfaz.FortuneService;

public class CricketCoachImpl implements Coach {
	
	private FortuneService fortuneService;
	
	//add new fields for emailaddress and team
	private String emailAddress;
	private String team;
	
	
	
	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		System.out.println("Inside the setter emailaddress method");
		this.emailAddress = emailAddress;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		System.out.println("Inside the setter team method");
		this.team = team;
	}

	public CricketCoachImpl() {
		System.out.println("Inside the no-constructor");
	}
	
	//our setterMethod
	public void setFortuneService(FortuneService fortuneService) {
		System.out.println("Inside the setter method");
		this.fortuneService = fortuneService;
	}

	@Override
	public String getDailyWorkout() {
		return "Practice fast bowling for 15 minutes";
	}

	@Override
	public int daysSpent() {
		return 3;
	}

	@Override
	public String getDailyFortune() {
		return this.fortuneService.getFortune();
	}

	@Override
	public String serveFortune() {
		return fortuneService.serveFortune();
	}
	
	

}
