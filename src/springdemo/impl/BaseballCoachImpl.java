package springdemo.impl;

import springdemo.interfaz.Coach;
import springdemo.interfaz.FortuneService;

public class BaseballCoachImpl implements Coach {
	
	private FortuneService fortuneService;
	
	public BaseballCoachImpl(FortuneService fortuneService) {
		this.fortuneService = fortuneService;
	}

	@Override
	public String getDailyWorkout() {
		return "Spend 30 minutes on batting practice";
	}

	@Override
	public int daysSpent() {
		return 1;
	}

	@Override
	public String getDailyFortune() {
		return fortuneService.getFortune();
	}

	@Override
	public String serveFortune() {
		return fortuneService.serveFortune();
	}

}
