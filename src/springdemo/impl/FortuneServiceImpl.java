package springdemo.impl;

import springdemo.interfaz.FortuneService;

public class FortuneServiceImpl implements FortuneService {

	@Override
	public String getFortune() {
		return "Today is your lucky day";
	}

	@Override
	public String serveFortune() {
		
		String[] cadenas = {"Primera cadena","Segunda cadena","Tercera cadena"};
		int random = (int) (Math.random()*(cadenas.length));
		return cadenas[random];
	}

}
