package springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import springdemo.interfaz.Coach;

public class BeanLifeCycleDemoApp {

	public static void main(String[] args) {
		
		//load the spring configuration file
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beanScope-applicationContext2.xml");
		
		// retrieve bean fromn the spring container
		Coach theCoach = context.getBean("myCoach", Coach.class);
		
		Coach alphaCoach = context.getBean("myCoach", Coach.class);
		
		// print out the reference of these beans
		boolean result = (theCoach == alphaCoach);
		System.out.println("Pointing to the same object? " + result);
		
		System.out.println("Memory allocation for theCoach: " + theCoach);
		
		System.out.println("Memory allocation for theCoach: " + alphaCoach);
		
		
		context.close();

	}

}
